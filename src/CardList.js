import React, { Component } from 'react';

import Window from '@zippytech/react-toolkit/Window'
import '@zippytech/react-toolkit/Window/index.css'

export default class CardList extends Component {

    renderCard(i) {
        
        return <Window  
            title="Zippy Toolkit"
            defaultPosition={{ top: 50, left: 27 }}
            defaultSize={{ width: '50%', height: 300 }}
        >
            Zippytech React Toolkit is designed to be a comprehensive set of rich UI components built with React and that can be easily integrated into existing or new applications.
            We've gone through a lot of iterations to make sure we provide a rich and flexible component set that is actually useful and help you speed-up app development.
            We focus on building components, so you can
        </Window>
    }

    renderCards() {
        const arr = Array.apply(null, Array(this.props.cards))

        return arr.map((nada, index) => {
            return this.renderCard(index)
        })
    }

    render() {
        return this.renderCards()
    }
}