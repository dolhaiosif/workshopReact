import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import CardList from './CardList.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to MD Project</h1>
        </header>
        <p className="App-intro">
          <CardList cards={10}/>
        </p>
      </div>
    );
  }
}

export default App;
